const express = require('express')
const app = express()
const env = require('env-var');
const sensor = require("node-dht-sensor");
const sensorModel = env.get('SENSOR_MODEL').default('22').asIntPositive()
sensor.setMaxRetries(10);
sensor.initialize(sensorModel, 4);

app.get('/getTemperature', async function(req, res){
  sensor.read(sensorModel, 4, function(err, temperature, humidity) {
    if (!err) {
      let tempFahrenheit = (temperature * (9/5)) + 32 
      res.send({temperature: tempFahrenheit, humidity: humidity})
    } else {
      res.sendStatus(500)
    }
  });
})

console.log(`Sensor model ${sensorModel}`)
app.listen(80, () => console.log(`App listening on port 80!`))